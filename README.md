This is the build protection plugin originally created for The Citadel (mc.thecitadel.us). 

**How Does it Work?**

First and foremost, Build Protector tracks all blocks that have been placed on
the map, and it makes it more difficult for other players to break blocks that
you placed. For example, a dirt block that you placed might only take you a
couple seconds to break, but it will take another player 10 seconds. The more
durable the block type, the longer it takes other players to break (configurable
in config.yml).

In addition, Build Protector provides the concept of alliances, allowing you to
group with other trusted members you know. That means you can access each
others' blocks and resources. But wait, we didnt stop there! Although players
can still destroy your build, we took into consideration that you don't want
them building in your surrounding area. That's where totems come in to action.
A totem gives you additional benefits in your territory. A totem consists of a
stack of:

    Obsidian
    Redstone Block
    Redstone Block
    Obsidian

Players who aren't in your alliance can't place blocks in your totem's perimeter,
and the totem will automatically lock all of your chests, furnaces, etc, to outsiders.
You can also control the spawning of hostile mobs in the area. Perhaps the most
exciting feature is that all alliance members have the opportunity to fly in the
perimeter of their alliance's totems.

Of course, other players can destroy your totem, leaving your territory vulnerable.
For this reason, you should place totems strategically, so that their territories
overlap each other. You might also want to bury your totems, or embed them in the
middle of your build, to make them more difficult to find.

Be aware, a compass will always point to the nearest totem, so raiders still have
an advantage.

Listed below are the basic commands that you need to know to use Build Protector:

**General Commands:**

    /alliance list - List all alliances
    /alliance info - Display info on your own alliance
    /alliance create [name] - Create a new alliance
    /alliance leave - Leave your alliance - If you are the leader, you can only leave your alliance if
    there are no other players in it. If you wish to leave anyway, you MUST set another player as the leader.

**Leader-only Commands:**

    /alliance setleader - Set the leader of the alliance to another player.
    /alliance invite - Invite another player to join your alliance.
    /alliance kick - Kick a player out of your alliance
    /alliance monsters - Toggle the spawning of hostile mobs within the perimeter of your alliance totems

**Admin-only Commands:**

    /alliance reload - Reload the config
    /alliance debug - Turn on debugging (will result in a lot of log messages!)
    /alliance setowner - This works with a WorldEdit selection, allowing you to manually set the owner
    of an area

**Dependencies:**

Vault, Essentials, Multiverse-Core, WorldEdit, Jedis, as well as Redis and MySQL
installed on your server.
