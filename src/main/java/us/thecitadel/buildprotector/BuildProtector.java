package us.thecitadel.buildprotector;

import redis.clients.jedis.Jedis;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.command.*;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.Random;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Chunk;
import org.bukkit.DyeColor;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Banner;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.block.Sign;
import org.bukkit.block.banner.Pattern;
import org.bukkit.event.Listener;
import org.bukkit.event.EventHandler;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockDamageEvent;
import org.bukkit.event.block.BlockFromToEvent;
import org.bukkit.event.entity.CreatureSpawnEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityExplodeEvent;
import org.bukkit.event.block.BlockMultiPlaceEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.block.BlockGrowEvent;
import org.bukkit.event.player.PlayerInteractAtEntityEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.event.player.PlayerTeleportEvent;
import org.bukkit.event.world.ChunkLoadEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.material.Attachable;
import org.bukkit.material.MaterialData;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.scheduler.BukkitScheduler;

import com.earth2me.essentials.Essentials;
import com.earth2me.essentials.User;
import com.sk89q.worldedit.bukkit.*;
import com.sk89q.worldedit.bukkit.selections.*;


public final class BuildProtector extends JavaPlugin implements Listener {
    List<String> Worlds = new ArrayList<String>();
    List<String> Alliance = new ArrayList<String>();
    List<String> Invincible = new ArrayList<String>();
    List<String> PreventMobs = new ArrayList<String>();
    List<Location> SuperTotem = new ArrayList<Location>();
    
    Hashtable<Block, Long> CurrentlyBreaking = new Hashtable<Block, Long>();
    Hashtable<String, Long> InvitationTime = new Hashtable<String, Long>();
    Hashtable<String, Long> tpCooldown = new Hashtable<String, Long>();
    Hashtable<String, String> PlayerAlliance = new Hashtable<String, String>();
    Hashtable<String, String> Leader = new Hashtable<String, String>();
    Hashtable<String, String> Invitation = new Hashtable<String, String>();
    Hashtable<String, Boolean> MobsEnabled = new Hashtable<String, Boolean>();
    Hashtable<String, Integer> Chunks = new Hashtable<String, Integer>();
    Hashtable<String, Location> InBorder = new Hashtable<String, Location>();
    Hashtable<Location, String> Totems = new Hashtable<Location, String>();

    Hashtable<Location, Long> RebuildList = new Hashtable<Location, Long>();
    Hashtable<Location, Material> RebuildMaterials = new Hashtable<Location, Material>();
    Hashtable<Location, MaterialData> RebuildData = new Hashtable<Location, MaterialData>();
    Hashtable<Location, Hashtable<Integer, String>> RebuildSignData = new Hashtable<Location, Hashtable<Integer, String>>();
    Hashtable<Location, Location> RebuildAttached = new Hashtable<Location, Location>();
    Hashtable<Location, DyeColor> RebuildBannerBaseColor = new Hashtable<Location, DyeColor>();
    Hashtable<Location, List<Pattern>> RebuildBannerPatterns = new Hashtable<Location, List<Pattern>>();
    
    Boolean debug = false;
    
    int default_time = 0;
    Double totem_radius = 0.0;
    Double totem_place_within = 0.0;
    Integer totem_max_per_player = 0;
    Double totem_min_distance = 0.0;
    WorldEditPlugin worldEdit = null;

    long second = 0;
    int tps = 0;
    
    String prefix = "";
    
    Jedis jedis = new Jedis("localhost");
    
	Connection conn = null;
	Statement st = null;
	PreparedStatement pst = null;

    @Override
    public void onEnable() {
        this.saveDefaultConfig();
        Bukkit.getServer().getPluginManager().registerEvents(this, this);
        
        this.loadSettings();

        jedis.set("BuildProtectorStatus", "Enabled");
    }
    
    public void loadSettings() {
    	log("loadSettings");
		this.reloadConfig();
	    
	    prefix = this.getConfig().getString("key-prefix");
		
		Worlds.clear();
        for (String w : this.getConfig().getStringList("worlds")) {
            Worlds.add(w);
        }
		
		Invincible.clear();
        for (String w : this.getConfig().getStringList("invincible")) {
        	Invincible.add(w);
        }
		
        PreventMobs.clear();
        for (String mob : this.getConfig().getStringList("prevent-mobs")) {
        	PreventMobs.add(mob);
        }
        
        debug = this.getConfig().getBoolean("debug", false);
        
		Alliance.clear();
		Leader.clear();
        PlayerAlliance.clear();
        Totems.clear();
        SuperTotem.clear();
        MobsEnabled.clear();
        try {
			conn = DriverManager.getConnection(getConfig().getString("database.url"), getConfig().getString("database.username"), getConfig().getString("database.password"));
	        pst = conn.prepareStatement("SELECT alliance_members.player, alliance.name FROM alliance INNER JOIN alliance_members ON alliance_members.alliance_id = alliance.alliance_id");
			ResultSet res = null;
	        res = pst.executeQuery();
	        while (res.next()) {
	        	String player = res.getString("player");                
	        	String alliance = res.getString("name");
	        	PlayerAlliance.put(player, alliance);
	        }

	        pst = conn.prepareStatement("SELECT alliance.name, alliance.leader, alliance.mobs_enabled FROM alliance");
	        res = null;
	        res = pst.executeQuery();
	        while (res.next()) {
	        	String alliance = res.getString("name");
	        	Alliance.add(alliance);
	        	Leader.put(alliance, res.getString("leader"));
	        	MobsEnabled.put(alliance, res.getBoolean("mobs_enabled"));
	        }
	        
	        pst = conn.prepareStatement("SELECT alliance.name, alliance_totems.world, alliance_totems.x, alliance_totems.y, alliance_totems.z, alliance_totems.super FROM alliance INNER JOIN alliance_totems ON alliance_totems.alliance_id = alliance.alliance_id");
			res = null;
	        res = pst.executeQuery();
	        while (res.next()) {
	        	String alliance = res.getString("name");
	        	World world = Bukkit.getWorld(res.getString("world"));
	        	Integer X = res.getInt("x");
	        	Integer Y = res.getInt("y");
	        	Integer Z = res.getInt("z");
	        	Location loc = new Location(world, (double) X, (double) Y, (double) Z);
	        	Totems.put(loc, alliance);
	        	
	        	if (res.getInt("super") > 0) {
	        		SuperTotem.add(loc);
	        	}
	        }
	        
		} catch (SQLException e) {
			log("SQL connection failed!", true);
			e.printStackTrace();
		} finally {
	    	try {
	        	pst.close();
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
        
    	default_time = this.getConfig().getInt("default-break-time");
        totem_radius = this.getConfig().getDouble("totem-radius");
        totem_place_within = this.getConfig().getDouble("totem-place-within");
        totem_max_per_player = this.getConfig().getInt("totem-max-per-player");
        totem_min_distance = this.getConfig().getDouble("totem-min-distance");
        
        worldEdit = (WorldEditPlugin) Bukkit.getServer().getPluginManager().getPlugin("WorldEdit");
        
		Bukkit.getScheduler().cancelTasks(this);
        BukkitScheduler scheduler = Bukkit.getServer().getScheduler();
        
        // Find closest totem to player and take action
        log("Starting player watcher...", true);
        scheduler.scheduleSyncRepeatingTask(this, new Runnable() {
            public void run() {
            	for (Player player : Bukkit.getServer().getOnlinePlayers()) {
        	    	User user = ((Essentials) Bukkit.getPluginManager().getPlugin("Essentials")).getUser(player.getName());
        	    	
        	        // Get the owner's alliance
        	    	String owner_alliance = player.getName();
        	        if (PlayerAlliance.containsKey(player.getName())) {
        	        	owner_alliance = PlayerAlliance.get(player.getName());
        	        } else {
        	        	// Player isn't in an alliance, so shouldn't be flying
	        	    	if (user.getBase().isFlying() && !user.isGodModeEnabled() && !user.getBase().isOp() && user.getBase().getGameMode() == GameMode.SURVIVAL) {
	        	    		log(player.getName() + " isn't in an alliance, so shouldn't be flying");
            				Integer time = (int) (player.getLocation().getBlockY() / 1.2);
        		            player.addPotionEffect(new PotionEffect(PotionEffectType.DAMAGE_RESISTANCE, time, 4), true);
	        	    		user.getBase().setFlying(false);
	        	    		user.getBase().setAllowFlight(false);
	        	    	}
        	        }
        	        
        	        // Find the closest totem
        	        Location closest = null;
        	    	for (Location loc : Totems.keySet()) {
        	    		if (player.getLocation().getWorld().equals(loc.getWorld()) && player.getLocation().distance(loc) < totem_radius) {
        	    			if (closest == null || player.getLocation().distance(loc) < player.getLocation().distance(closest)) {
        	    				closest = loc;
        	    			}
        	    		}
        	    	}
        	        // Check nearest totem
        	        if (InBorder.containsKey(player.getName())) {
        	        	Location totem_loc = InBorder.get(player.getName());
        				World w = player.getWorld();
        				if (!totem_loc.getWorld().equals(w)) {
        					InBorder.remove(player.getName());
            	        	// Player in wrong world - shouldn't be flying
    	        	    	if (user.getBase().isFlying() && !user.isGodModeEnabled() && !user.getBase().isOp() && user.getBase().getGameMode() == GameMode.SURVIVAL) {
		        	    		log("Player in wrong world - " + player.getName() + " shouldn't be flying");
	            				Integer time = (int) (player.getLocation().getBlockY() / 1.2);
	        		            player.addPotionEffect(new PotionEffect(PotionEffectType.DAMAGE_RESISTANCE, time, 4), true);
		        	    		user.getBase().setFlying(false);
		        	    		user.getBase().setAllowFlight(false);
    	        	    	}
        				} else {
            	        	Block totem = w.getBlockAt(totem_loc.getBlockX(), totem_loc.getBlockY(), totem_loc.getBlockZ());
        					if (!isOwner(player, totem)) {
                	        	// Nearest totem not in player's alliance, so shouldn't be flying
        	        	    	if (user.getBase().isFlying() && !user.isGodModeEnabled() && !user.getBase().isOp() && user.getBase().getGameMode() == GameMode.SURVIVAL) {
            	    				log("Nearest totem not in player's alliance - " + player.getName() + " shouldn't be flying");
            	    				player.sendMessage(ChatColor.RED + "You entered the territory of " + ChatColor.GOLD + Totems.get(totem));

	                    			for (Player p : Bukkit.getServer().getOnlinePlayers()) {
	                    			    if (!p.getName().equalsIgnoreCase(player.getName()) && PlayerAlliance.containsKey(p.getName())) {
	            	    					if (PlayerAlliance.get(p.getName()).equalsIgnoreCase(Totems.get(closest))) {
	            	    						p.sendMessage(ChatColor.GOLD + user.getName() + ChatColor.RED + " has entered your territory!");
	                    	    				log("Notifying " + p.getName());
	            	    					}
	                    			    }
	                    			}
            	    				
                    				Integer time = (int) (player.getLocation().getBlockY() / 1.2);
                		            player.addPotionEffect(new PotionEffect(PotionEffectType.DAMAGE_RESISTANCE, time, 4), true);
        	        	    		user.getBase().setFlying(false);
        	        	    		user.getBase().setAllowFlight(false);
        	        	    	}
        					}
        				}
        	        } else {
        	        	// If player is flying but they don't have an associated totem, they must have teleported to the wilderness
	        	    	if (user.getBase().isFlying() && !user.isGodModeEnabled() && !user.getBase().isOp() && user.getBase().getGameMode() == GameMode.SURVIVAL) {
	        	    		log("Teleport to wilderness - " + player.getName() + " shouldn't be flying");
            				Integer time = (int) (player.getLocation().getBlockY() / 1.2);
        		            player.addPotionEffect(new PotionEffect(PotionEffectType.DAMAGE_RESISTANCE, time, 4), true);
	        	    		user.getBase().setFlying(false);
	        	    		user.getBase().setAllowFlight(false);
	        	    	}
        	        }
        	    	// If there's a new closest totem, set it
        	    	if (closest != null ) {
        	    		Boolean doset = false;
    	    			if (!InBorder.containsKey(player.getName())) {
    	    				player.sendMessage(ChatColor.RED + "You entered the territory of " + ChatColor.GOLD + Totems.get(closest));
    	    				history(player.getName(), "entered territory", Totems.get(closest));
                			for (Player p : Bukkit.getServer().getOnlinePlayers()) {
                			    if (!p.getName().equalsIgnoreCase(player.getName()) && PlayerAlliance.containsKey(p.getName())) {
        	    					if (PlayerAlliance.get(p.getName()).equalsIgnoreCase(Totems.get(closest))) {
        	    						p.sendMessage(ChatColor.GOLD + user.getName() + ChatColor.RED + " has entered your territory!");
                	    				log("Notifying " + p.getName());
        	    					}
                			    }
                			}
    	    				
        	    			doset = true;
    	    			} else if (!InBorder.get(player.getName()).equals(closest)) {
    	    				doset = true;
    	    			}
    	    			if (doset) {
    	    				log(player.getName() + " in " + closest.toString() + " radius");
            	    		InBorder.put(player.getName(), closest);
    	    				player.setCompassTarget(closest);
        	    			if (PlayerAlliance.containsKey(player.getName()) && Totems.get(closest).equalsIgnoreCase(owner_alliance)) {
        	        	    	if (!user.isGodModeEnabled() && !user.getBase().isOp() && user.getBase().getGameMode() == GameMode.SURVIVAL) {
	        	        	    	user.getBase().setFallDistance(0f);
	        	        	    	user.getBase().setAllowFlight(true);
        	        	    	}
        	    			} else {
        	        	    	if (user.getBase().isFlying() && !user.isGodModeEnabled() && !user.getBase().isOp() && user.getBase().getGameMode() == GameMode.SURVIVAL) {
    		        	    		log("Nearest totem not in player's alliance - " + player.getName() + " shouldn't be flying");
    	            				Integer time = (int) (player.getLocation().getBlockY() / 1.2);
    	        		            player.addPotionEffect(new PotionEffect(PotionEffectType.DAMAGE_RESISTANCE, time, 4), true);
    		        	    		user.getBase().setFlying(false);
    		        	    		user.getBase().setAllowFlight(false);
        	        	    	}
        	    			}
    	    			}
        	    	}
        	    	// See if they are outside of the totem's range
                	if (InBorder.containsKey(player.getName())) {
            			Location loc = InBorder.get(player.getName());
            			if (player.getLocation().getWorld().equals(loc.getWorld())) {
	                		if (!Totems.containsKey(loc) || player.getLocation().distance(InBorder.get(player.getName())) > totem_radius) {
	                			InBorder.remove(player.getName());
	                			player.setCompassTarget(player.getWorld().getSpawnLocation());
	            				player.sendMessage(ChatColor.DARK_GREEN + "- Wilderness -");
	            				Integer time = (int) (player.getLocation().getBlockY() / 1.2);
	        		            player.addPotionEffect(new PotionEffect(PotionEffectType.DAMAGE_RESISTANCE, time, 4), true);
        	        	    	if (!user.isGodModeEnabled() && !user.getBase().isOp() && user.getBase().getGameMode() == GameMode.SURVIVAL) {
		    	        	    	user.getBase().setFlying(false);
		    	        	    	user.getBase().setAllowFlight(false);
        	        	    	}
	                		}
            			} else {
                			InBorder.remove(player.getName());
                			player.setCompassTarget(player.getWorld().getSpawnLocation());
            				player.sendMessage(ChatColor.DARK_GREEN + "- Wilderness -");
            				Integer time = (int) (player.getLocation().getBlockY() / 1.2);
        		            player.addPotionEffect(new PotionEffect(PotionEffectType.DAMAGE_RESISTANCE, time, 4), true);
    	        	    	if (!user.isGodModeEnabled() && !user.getBase().isOp() && user.getBase().getGameMode() == GameMode.SURVIVAL) {
	    	        	    	user.getBase().setFlying(false);
	    	        	    	user.getBase().setAllowFlight(false);
    	        	    	}
            			}
                	}
            	}
            }
        }, 100L, 100L);
        
        // Remove mobs in totem area effect
        log("Starting mob removal process...", true);
        scheduler.scheduleSyncRepeatingTask(this, new Runnable() {
            public void run() {
                for (String w : Worlds) {
                	if (!Worlds.contains(w.toString())) break;
                	if (w.toString().contains("_nether") || w.toString().contains("_the_end")) break;
	            	for (Entity mob : Bukkit.getServer().getWorld(w).getEntities()) {
	            		Location mob_loc = mob.getLocation();
	                	for (Location loc : Totems.keySet()) {
	                		if (MobsEnabled.containsKey(Totems.get(loc)) && !MobsEnabled.get(Totems.get(loc))) {
	            	    		if (mob_loc.getWorld().equals(loc.getWorld()) && mob_loc.distance(loc) < totem_radius) {
	            	    			EntityType mob_type = mob.getType();
	            	    	        for (String mt : PreventMobs) {
	            	    	        	if (mob_type.toString().equalsIgnoreCase(mt)) {
	            	    	        		mob.remove();
	            	    	        	}
	            	    	        }
	            	    		}
	                		}
	                	}
	            	}
                }
            }
        }, 120L, 100L);
        
        // Remove totem records where the totem no longer exists
        // (this will take care of exploded totems, etc)
//        log("Starting totem watcher...", true);
//        scheduler.scheduleSyncRepeatingTask(this, new Runnable() {
//            public void run() {
//                List<Location> locs = new ArrayList<Location>();
//            	for (Location loc : Totems.keySet()) {
//            		if (Bukkit.getServer().getWorlds().contains(loc.getWorld())) {
//	            		World w = Bukkit.getServer().getWorld(loc.getWorld().getName());
//	                	Block totem = w.getBlockAt(loc.getBlockX(), loc.getBlockY(), loc.getBlockZ());
//	                	Block above1 = w.getBlockAt(loc.getBlockX(), loc.getBlockY()+1, loc.getBlockZ());
//	                	Block above2 = w.getBlockAt(loc.getBlockX(), loc.getBlockY()+2, loc.getBlockZ());
//	                	Block above3 = w.getBlockAt(loc.getBlockX(), loc.getBlockY()+3, loc.getBlockZ());
//	            		if (!totem.getType().equals(Material.OBSIDIAN) || !above1.getType().equals(Material.REDSTONE_BLOCK) || !above2.getType().equals(Material.REDSTONE_BLOCK) || !above3.getType().equals(Material.OBSIDIAN)) {
//	            			locs.add(loc);
//	            		}
//            		}
//            	}
//            	for (Location loc : locs) {
//    	    		log("Totem is missing at " + loc.toString() + ". Removing the record.");
//    	    		if (Totems.containsKey(loc)) {
//                		World w = Bukkit.getServer().getWorld(loc.getWorld().getName());
//    	    			try {
//    		    			conn = DriverManager.getConnection(getConfig().getString("database.url"), getConfig().getString("database.username"), getConfig().getString("database.password"));
//    			        	pst = conn.prepareStatement("DELETE FROM alliance_totems WHERE world = ? AND x = ? AND y = ? AND z = ?");
//    				        pst.setString(1, w.getName());
//    				        pst.setInt(2, loc.getBlockX());
//    				        pst.setInt(3, loc.getBlockY());
//    				        pst.setInt(4, loc.getBlockZ());
//    				        pst.executeUpdate();
//    		    		} catch (SQLException e) {
//    		    			log("SQL connection failed!", true);
//    		    			e.printStackTrace();
//    		    		} finally {
//    		    	    	try {
//    		    	        	pst.close();
//    		    				conn.close();
//    		    			} catch (SQLException e) {
//    		    				e.printStackTrace();
//    		    			}
//    		    		}
//    	    			Totems.remove(loc);
//    	    		}
//            	}
//            }
//        }, 160L, 200L);
        
        // Rebuild after an explosion
        log("Starting explosion rebuilder...", true);
        scheduler.scheduleSyncRepeatingTask(this, new Runnable() {
            @SuppressWarnings("deprecation")
			public void run() {
            	if (getConfig().getBoolean("rebuild-after-explosions", false)) {
            		if (RebuildList.size() > 0) {
	            		int idx = new Random().nextInt(RebuildList.size());
	            		int count = 0;
	            		for (Location loc : RebuildList.keySet()) {
	            			if (count == idx) {
	            				if (System.currentTimeMillis() - RebuildList.get(loc) > (getConfig().getInt("rebuild-pause", 30) * 1000)) {
	            					Boolean skip = false;
	            					if (RebuildAttached.containsKey(loc)) {
	            						if (RebuildList.containsKey(RebuildAttached.get(loc))) {
	            							skip = true;
	            						}
	            					}
	            					if (!skip) {
			    	            		loc.getBlock().setType(RebuildMaterials.get(loc));
			    	            		loc.getBlock().setData(RebuildData.get(loc).getData());
			    	            		
			    	            		if (RebuildMaterials.get(loc) == Material.SIGN || RebuildMaterials.get(loc) == Material.WALL_SIGN || RebuildMaterials.get(loc) == Material.SIGN_POST) {
			    	            			Sign sign = (Sign) loc.getBlock().getState();
			    	            			Hashtable<Integer, String> line = RebuildSignData.get(loc);
			    	            			sign.setLine(0, line.get(0));
			    	            			sign.setLine(1, line.get(1));
			    	            			sign.setLine(2, line.get(2));
			    	            			sign.setLine(3, line.get(3));
			    	            			sign.update();
			    	            			RebuildSignData.remove(loc);
			    	            		}
			    	            		
			    	            		if (RebuildMaterials.get(loc) == Material.BANNER || RebuildMaterials.get(loc) == Material.STANDING_BANNER || RebuildMaterials.get(loc) == Material.WALL_BANNER) {
			    	            			Banner banner = (Banner) loc.getBlock().getState();
			    	            			banner.setBaseColor(RebuildBannerBaseColor.get(loc));
			    	            			banner.setPatterns(RebuildBannerPatterns.get(loc));
			    	            			banner.update();
			    	            			RebuildBannerBaseColor.remove(loc);
			    	            			RebuildBannerPatterns.remove(loc);
			    	            		}
			    	            		
			    	            		RebuildMaterials.remove(loc);
			    	            		RebuildData.remove(loc);
			    	            		RebuildList.remove(loc);
			    	            		break;
	            					}
	            				}
	            			}
	            			count += 1;
	            		}
            		}
            	}
            }
        }, 100L, getConfig().getLong("rebuild-increment", 20));
        
        // Watch TPS
        scheduler.scheduleSyncRepeatingTask(this, new Runnable() {
            long sec;
            int ticks;
           
            public void run() {
                sec = (System.currentTimeMillis() / 1000);
                if (second == sec) {
                    ticks++;
                } else {
                    second = sec;
                    tps = (tps == 0 ? ticks : ((tps + ticks) / 2));
                    ticks = 0;
                    if (tps < 13) {
                    	log("Lag detected! TPS: " + tps, true);
                    	for (String player : Chunks.keySet()) {
                    		if (Chunks.get(player) > 5) {
                        		log(player + ": " + Chunks.get(player), true);
                    		}
                    	}
                    }
                	for (String player : Chunks.keySet()) {
                		Chunks.put(player, (int) Chunks.get(player) / 2);
                	}
                }
            }
        }, 100, 1);
    }
    
    @Override
    public void onDisable() {
        jedis.set("BuildProtectorStatus", "Disabled");
        jedis.close();	
    }

    @EventHandler
    public void onBlockPlace(BlockPlaceEvent event) {
        Player player = event.getPlayer();

        if (opBypass(player.getName())) return;
        
        if (tpCooldown(player.getName())) {
        	event.setCancelled(true);
        	return;
        }
        
        Block block = event.getBlock();
    	log(player.getName() + " placed " + block.getType().toString());
        String world = player.getWorld().getName();
        if (Worlds.contains(world)) {
	        int X = block.getX();
	        int Y = block.getY();
	        int Z = block.getZ();
	
	        String playerkey = prefix + "-" + world + "." + X + "." + Y + "." + Z;
	        log("Block Place: " + playerkey + " " + player.getName());
	        jedis.set(playerkey, player.getName());
        }
        
        // Get the owner's alliance
    	String owner_alliance = player.getName();
        if (PlayerAlliance.containsKey(player.getName())) {
        	owner_alliance = PlayerAlliance.get(player.getName());
        }
        
        // If the player is within someone else's totem radius, deny the block placement
        if (InBorder.containsKey(player.getName())) {
        	Location loc = InBorder.get(player.getName());
        	if (Totems.containsKey(loc) && !Totems.get(loc).equalsIgnoreCase(owner_alliance)) {
    			event.setCancelled(true);
    		}
        }
        
        // Detect when a totem is built
        Block base = isTotem(block);
        Block totem = base;
    	
        // Check if the totem is too close to another
        if (totem != null) {
        	// How many totems has this player's alliance built?
        	Integer alliance_totems = 0;
        	for (Location loc : Totems.keySet()) {
    			if (Totems.get(loc).equalsIgnoreCase(owner_alliance)) {
    				alliance_totems = alliance_totems + 1;
    			}
        	}
        	Integer alliance_members = 0;
        	for (String p : PlayerAlliance.keySet()) {
        		if (PlayerAlliance.get(p).equalsIgnoreCase(owner_alliance)) {
        			alliance_members = alliance_members + 1;
        		}
        	}
        	Integer player_totems = ((int) alliance_members * (int) totem_max_per_player);
        	log("Alliance Totems: " + alliance_totems);
        	log("Alliance Members: " + alliance_members);
        	log("Player Totems: " + player_totems);
        	if (alliance_totems > 0 && alliance_totems >= player_totems) {
	        	player.sendMessage(ChatColor.RED + "You are at your maximum of " + totem_max_per_player + " totems per player in your alliance!");
				totem = null;
        	}
        	
        	if (totem != null) {
	            // Find the closest totem
	            Location closest = null;
	        	for (Location loc : Totems.keySet()) {
	        		if (totem.getLocation().getWorld().equals(loc.getWorld())) {
	        			if (closest == null || player.getLocation().distance(loc) < player.getLocation().distance(closest)) {
	        				closest = loc;
	        			}
	        		}
	        	}
	        	if (closest != null) {
	    			if (Totems.get(closest).equalsIgnoreCase(owner_alliance)) {
	    				// closest totem is in player's alliance
	    				if (totem.getLocation().distance(closest) > totem_place_within) {
				        	player.sendMessage(ChatColor.RED + "You are too far from your other totems!");
				        	player.sendMessage(ChatColor.GRAY + "(Totems must be placed within " + totem_place_within + " blocks of each other)");
	    					totem = null;
	    				}
	        		} else {
	    				// closest totem is not in player's alliance
	    				if (totem.getLocation().distance(closest) < totem_min_distance) {
				        	player.sendMessage(ChatColor.RED + "You are too close to another alliance's totem!");
		        			totem = null;
	        			}
	        		}
	        	}
        	}
        }
        // Save the totem
        if (totem != null && !Totems.contains(totem.getLocation())) {
	        if (PlayerAlliance.containsKey(player.getName())) {
	        	try {
					conn = DriverManager.getConnection(getConfig().getString("database.url"), getConfig().getString("database.username"), getConfig().getString("database.password"));
			        pst = conn.prepareStatement("SELECT alliance_id FROM alliance WHERE name = ?");
			        pst.setString(1, owner_alliance);
					ResultSet res = null;
			        res = pst.executeQuery();
			        while (res.next()) {           
			        	Integer alliance_id = res.getInt("alliance_id");

				        pst = conn.prepareStatement("REPLACE INTO alliance_totems SET alliance_id = ?, world = ?, x = ?, y = ?, z = ?");
				        pst.setInt(1, alliance_id);
				        pst.setString(2, world);
				        pst.setInt(3, totem.getX());
				        pst.setInt(4, totem.getY());
				        pst.setInt(5, totem.getZ());
				        pst.executeUpdate();

			        	Totems.put(totem.getLocation(), owner_alliance);
			        	player.sendMessage(ChatColor.GREEN + "Totem created for " + ChatColor.GOLD + owner_alliance + ChatColor.GREEN + "!");
			        }
	        	} catch (SQLException e) {
	    			log("SQL connection failed!", true);
	    			e.printStackTrace();
	    		} finally {
	    	    	try {
	    	        	pst.close();
	    				conn.close();
	    			} catch (SQLException e) {
	    				e.printStackTrace();
	    			}
	    		}
	        } else {
	        	player.sendMessage(ChatColor.RED + "Totems can only be created if you are in an alliance!");
	        	totem = null;
	        }
        }
        
        // destroy the totem
        if (base != null && totem == null) {
			World w = player.getWorld();
        	Block above1 = w.getBlockAt(base.getX(), base.getY()+1, base.getZ());
        	Block above2 = w.getBlockAt(base.getX(), base.getY()+2, base.getZ());
        	Block above3 = w.getBlockAt(base.getX(), base.getY()+3, base.getZ());
        	base.breakNaturally();
			above1.breakNaturally();
			above2.breakNaturally();
			above3.breakNaturally();
        }
    }

    @EventHandler
    public void onBlockBreak(BlockBreakEvent event) {
        Player player = event.getPlayer();
        
        if (opBypass(player.getName())) return;

        Block block = event.getBlock();
        
        if (!isOwner(player, block)) {
        	if (Invincible.contains(block.getType())) {
        		log(player.getName() + ": Trying to break invincible block");
        		event.setCancelled(true);
        		return;
        	}

            Hashtable<String, String> owner = getOwner(block);
        	for (Location loc : Totems.keySet()) {
        		if (!Totems.get(loc).equalsIgnoreCase(owner.get("alliance")) && block.getLocation().getWorld().equals(loc.getWorld()) && block.getLocation().distance(loc) < totem_radius) {
    	    		log(player.getName() + ": Blocking damage to " + block.getType().toString());
    	    		player.sendMessage(ChatColor.RED + "This is protected by a nearby totem!");
    	    		event.setCancelled(true);
    	    		break;
        		}
        	}
        }
        
        String world = player.getWorld().getName();
        if (Worlds.contains(world)) {
	        int X = block.getX();
	        int Y = block.getY();
	        int Z = block.getZ();
	
	        String playerkey = prefix + "-" + world + "." + X + "." + Y + "." + Z;
	        log(player.getName() + ": Block Break: " + playerkey);
	        jedis.del(playerkey);
        }
       

        // Did this block break result in a totem being broken?
        List<Location> rem_keys = new ArrayList<Location>();
		for (Location loc : Totems.keySet()) {
			if (block.getLocation().getWorld().equals(loc.getWorld()) && block.getLocation().distance(loc) < 5) {
	            World w = loc.getWorld();
	        	Block totem1 = w.getBlockAt(loc.getBlockX(), loc.getBlockY(), loc.getBlockZ());
	        	Block totem2 = w.getBlockAt(loc.getBlockX(), loc.getBlockY()+1, loc.getBlockZ());
	        	Block totem3 = w.getBlockAt(loc.getBlockX(), loc.getBlockY()+2, loc.getBlockZ());
	        	if (block.getLocation().equals(totem1.getLocation()) || block.getLocation().equals(totem2.getLocation()) || block.getLocation().equals(totem3.getLocation())) {
	        		try {
		    			conn = DriverManager.getConnection(getConfig().getString("database.url"), getConfig().getString("database.username"), getConfig().getString("database.password"));
			        	pst = conn.prepareStatement("DELETE FROM alliance_totems WHERE world = ? AND x = ? AND y = ? AND z = ?");
				        pst.setString(1, w.getName());
				        pst.setInt(2, loc.getBlockX());
				        pst.setInt(3, loc.getBlockY());
				        pst.setInt(4, loc.getBlockZ());
				        pst.executeUpdate();
			        	rem_keys.add(loc);
			        	player.sendMessage(ChatColor.RED + "Totem broken!");
		    		} catch (SQLException e) {
		    			log("SQL connection failed!", true);
		    			e.printStackTrace();
		    		} finally {
		    	    	try {
		    	        	pst.close();
		    				conn.close();
		    			} catch (SQLException e) {
		    				e.printStackTrace();
		    			}
		    		}
		        }
			}
		}
		for (Location loc : rem_keys) {
        	Totems.remove(loc);
		}
    }
    
    @EventHandler
    public void onChunkLoad(ChunkLoadEvent event) {
    	Chunk chunk = event.getChunk();
    	if (event.isNewChunk()) {
        	int cX = chunk.getX();
        	int cZ = chunk.getZ();
        	Location cloc = new Location(chunk.getWorld(), (double) cX, (double) 65, (double) cZ);
        	
        	Player closest = null;
        	double closest_dist = 99999;
        	for (Player player : Bukkit.getServer().getOnlinePlayers()) {
        		if (player.getWorld().equals(chunk.getWorld())) {
	        		Location ploc = player.getLocation();
	        		double dist = cloc.distance(ploc);
	        		if (dist < closest_dist) {
	        			closest_dist = dist;
	        			closest = player;
	        		}
        		}
        	}
        	if (closest != null) {
        		if (!Chunks.containsKey(closest.getName())) {
        			Chunks.put(closest.getName(), 0);
        		}
        		Chunks.put(closest.getName(), Chunks.get(closest.getName()) + 1);
        	}
    	}
    }
    
    @EventHandler
    public void onEntityDamage(EntityDamageEvent event) {
    	if (event instanceof EntityDamageByEntityEvent) {
    		EntityDamageByEntityEvent damageEvent = (EntityDamageByEntityEvent)event;

        	Entity damager = damageEvent.getDamager();
        	Entity damagee = damageEvent.getEntity();

            if (opBypass(damager.getName())) return;
        	
        	// If it's a hostile mob, allow the damage
        	if (PreventMobs.contains(damagee.getType().toString())) {
        		return;
        	}

            // Get the owner's alliance
        	String player_alliance = damager.getName();
            if (PlayerAlliance.containsKey(damager.getName())) {
            	player_alliance = PlayerAlliance.get(damager.getName());
            }

            if (!damagee.getType().equals(EntityType.PLAYER)) {
	        	for (Location loc : Totems.keySet()) {
	        		if (!Totems.get(loc).equalsIgnoreCase(player_alliance) && damagee.getLocation().getWorld().equals(loc.getWorld()) && damagee.getLocation().distance(loc) < totem_radius) {
	    	    		log("Blocking damage to " + damagee.getType().toString());
	    	    		damager.sendMessage(ChatColor.RED + "This is protected by a nearby totem!");
	    	    		event.setCancelled(true);
	    	    		break;
	        		}
	        	}
            }
    	}
    }
    
    @EventHandler
    public void onPlayerInteract(PlayerInteractEvent event) {
    	Player player = event.getPlayer();
    	
        if (opBypass(player.getName())) return;
        
        if (tpCooldown(player.getName())) {
        	event.setCancelled(true);
        	return;
        }
        
    	Material material = event.getMaterial();
    	Block block = event.getClickedBlock();
    	if (block == null) return;
        
        // Get the owner's alliance
    	String owner_alliance = player.getName();
        if (PlayerAlliance.containsKey(player.getName())) {
        	owner_alliance = PlayerAlliance.get(player.getName());
        }

        Boolean cancel = false;

    	for (Location loc : Totems.keySet()) {
    		if (!Totems.get(loc).equalsIgnoreCase(owner_alliance) && block.getLocation().getWorld().equals(loc.getWorld()) && block.getLocation().distance(loc) < totem_radius) {
	    		if (material == Material.WATER_BUCKET || material == Material.LAVA_BUCKET) {
            		player.sendMessage(ChatColor.RED + "Sorry, you can only place that in your alliance's territory!");
            		player.getInventory().setItem(player.getInventory().getHeldItemSlot(), new ItemStack(material, 1));
        			cancel = true;
        			break;
	        	}
	        	if (event.getAction() == Action.PHYSICAL) {
        			cancel = true;
        			break;
	        	}
	        	if (event.getAction() == Action.RIGHT_CLICK_BLOCK || event.getAction() == Action.LEFT_CLICK_BLOCK) {
		            for (String blocktype : this.getConfig().getStringList("locked")) {
			        	if (block.getType().toString().contains(blocktype) && !block.getType().equals(Material.ENDER_CHEST)) {
		            		player.sendMessage(ChatColor.RED + "This is protected by a nearby totem!");
		        			cancel = true;
		        			break;
			        	}
		            }
	        	}
    		}
    	}

		event.setCancelled(cancel);
    }
    
    @EventHandler
    public void onPlayerInteractAtEntity(PlayerInteractAtEntityEvent event) {
    	Player player = event.getPlayer();
    	Entity ent = event.getRightClicked();
    	
        if (opBypass(player.getName())) return;
        
        if (tpCooldown(player.getName())) {
        	event.setCancelled(true);
        	return;
        }
        
        // Get the owner's alliance
    	String owner_alliance = player.getName();
        if (PlayerAlliance.containsKey(player.getName())) {
        	owner_alliance = PlayerAlliance.get(player.getName());
        }

    	for (Location loc : Totems.keySet()) {
    		if (!Totems.get(loc).equalsIgnoreCase(owner_alliance) && ent.getLocation().getWorld().equals(loc.getWorld()) && ent.getLocation().distance(loc) < totem_radius) {
    			event.setCancelled(true);
    			break;
    		}
    	}
    }
    
    @EventHandler
    public void onBlockFromTo(BlockFromToEvent event) {
    	// water and lava
    }
    
    @EventHandler
    public void onBlockGrow(BlockGrowEvent event) {
    	// plant grow
    }
    
	@EventHandler
    public void onEntityExplode(EntityExplodeEvent event) {
    	if (event.getEntityType() == EntityType.PRIMED_TNT || event.getEntityType() == EntityType.CREEPER || event.getEntityType() == EntityType.ENDER_CRYSTAL || event.getEntityType() == EntityType.WITHER) {
    		event.setYield(0);
	    	for (Block b : event.blockList()) {
	    		Location loc = b.getLocation();
	    		Material mat = b.getType();
	    		MaterialData data = b.getState().getData();
	    		
	    		if (!b.getType().toString().equalsIgnoreCase("AIR") && !b.getType().toString().equalsIgnoreCase("TNT")) {
	    			RebuildList.put(loc, System.currentTimeMillis());
	    			RebuildMaterials.put(loc, mat);
	    			RebuildData.put(loc, data);

		    		if (b.getType() == Material.SIGN || b.getType() == Material.SIGN_POST || b.getType() == Material.WALL_SIGN) {
		    			Sign sign = (Sign) b.getState();
		    			Hashtable<Integer, String> signdata = new Hashtable<Integer, String>();
		    			signdata.put(0, sign.getLine(0));
		    			signdata.put(1, sign.getLine(1));
		    			signdata.put(2, sign.getLine(2));
		    			signdata.put(3, sign.getLine(3));
		    			RebuildSignData.put(loc, signdata);
		    		}
		    		
		    		if (b.getType() == Material.BANNER || b.getType() == Material.STANDING_BANNER || b.getType() == Material.WALL_BANNER) {
			    		Banner banner = (Banner) b.getState();
			    		RebuildBannerBaseColor.put(loc, banner.getBaseColor());
			    		RebuildBannerPatterns.put(loc, banner.getPatterns());
		    		}
	    			
		    		if (data instanceof Attachable) {
		    			BlockFace face = ((Attachable) data).getAttachedFace();
		    			Block attachedTo = b.getRelative(face);
		    			RebuildAttached.put(loc, attachedTo.getLocation());
		    		}
	    		}
	    	}
    	}
    }
    
    @EventHandler
    public void onBlockMultiPlace(BlockMultiPlaceEvent event) {
    	log("Multiplace event");
    }
    
    @EventHandler
    public void onBlockDamage(BlockDamageEvent event) {
    	log("Block damage");
        Player player = event.getPlayer();
        
        if (opBypass(player.getName())) return;

        Block block = event.getBlock();
        
        if (!isOwner(player, block)) {
        	Hashtable<String, String> owner = getOwner(block);
        	
        	log("Not owner!");
        	if (Invincible.contains(block.getType().toString())) {
        		log("(Damage) Invincible block");
        		event.setCancelled(true);
	            player.addPotionEffect(new PotionEffect(PotionEffectType.SLOW_DIGGING, 86400, 4), true);
	            player.sendMessage(ChatColor.RED + owner.get("owner") + " owns that block, and it cannot be broken!");
        		return;
        	}

            for (String blocktype : this.getConfig().getStringList("locked")) {
            	if (block.getType().toString().contains(blocktype)) {
            		player.sendMessage(ChatColor.RED + "This is protected by a nearby totem!");
        			event.setCancelled(true);
        			return;
            	}
            }
        
	        String world = player.getWorld().getName();
	        if (Worlds.contains(world)) {
		    	// Get the player's alliance
		    	String player_alliance = "";
		        if (PlayerAlliance.containsKey(player.getName())) {
		        	player_alliance = PlayerAlliance.get(player.getName());
		        } else {
		        	player_alliance = player.getName();
		        }
		    	
		        if (!owner.get("owner").equals("") && !player.getName().equalsIgnoreCase(owner.get("owner")) && !player_alliance.equalsIgnoreCase(owner.get("alliance"))) {
		            int secs = 0;
		            int time = this.getConfig().getInt("break-times." + block.getType().toString());
		            if (time > 0) {
		                secs = time;
		            	time = time * 10;
		            } else {
		                secs = default_time;
		            	time = default_time * 10;
		            }
		            
		            log(block.getType().toString() + " hit by " + player.getName() + " at " + block.getX() + " " + block.getY() + " " + block.getZ() + " in " + world + ". Owner is " + owner.get("owner") + "! Slowing for " + secs + " seconds.");
	                
		            CurrentlyBreaking.put(block, System.currentTimeMillis());
		            player.addPotionEffect(new PotionEffect(PotionEffectType.SLOW_DIGGING, time, 4), true);
		            //player.addPotionEffect(new PotionEffect(PotionEffectType.BLINDNESS, time, 1), true);
		            player.sendMessage(ChatColor.RED + owner.get("owner") + " owns that block! It's going to take " + secs + " seconds to break.");
		        } else if (!owner.get("owner").equals("") && player.getName().equalsIgnoreCase(owner.get("owner"))) {
	                CurrentlyBreaking.remove(block);
		            player.addPotionEffect(new PotionEffect(PotionEffectType.SLOW_DIGGING, 0, 0), true);
		            //player.addPotionEffect(new PotionEffect(PotionEffectType.BLINDNESS, 0, 0), true);
		        }
	        }
        }
    }

//    @EventHandler
//    public void onBlockPistonExtend(BlockPistonExtendEvent event) {
//    	log("Block piston extend event");
//    	Block block = event.getBlock();
//        World world = block.getWorld();
//        
//    	Location loc = block.getLocation();
//        int X = loc.getBlockX();
//        int Y = loc.getBlockY();
//        int Z = loc.getBlockZ();
//        String pistonkey = prefix + "-" + world.getName() + "." + X + "." + Y + "." + Z;
//        String pistonowner = jedis.get(pistonkey);
//        log("Piston Check: " + pistonkey + "  Owner: " + pistonowner);
//        
//    	// Get the piston's alliance
//    	String pistonalliance = "";
//        if (pistonowner != null && PlayerAlliance.containsKey(pistonowner)) {
//        	pistonalliance = PlayerAlliance.get(pistonowner);
//        } else {
//        	pistonalliance = pistonowner;
//        }
//        
//        Boolean cancel = false;
//        for (Block bl : event.getBlocks()) {
//        	Location bloc = bl.getLocation();
//	        int bX = bloc.getBlockX();
//	        int bY = bloc.getBlockY();
//	        int bZ = bloc.getBlockZ();
//	        
//	        String blockkey = prefix + "-" + world.getName() + "." + bX + "." + bY + "." + bZ;
//	        String blockowner = jedis.get(blockkey);
//	        log("Block Check: " + blockkey + "  Owner: " + blockowner);
//	        
//	    	// Get the block's alliance
//	    	String blockalliance = "";
//	        if (blockowner != null && PlayerAlliance.containsKey(blockowner)) {
//	        	blockalliance = PlayerAlliance.get(blockowner);
//	        } else {
//	        	blockalliance = blockowner;
//	        }
//	        
//	        if (blockalliance != null && !pistonalliance.equalsIgnoreCase(blockalliance)) {
//	        	cancel = true;
//	        	log(pistonalliance + " is trying to move " + blockalliance + "'s block with a piston. Cancelling event!");
//	        }
//        }
//        event.setCancelled(cancel);
//    }
//
//    @EventHandler
//    public void onBlockPistonRetract(BlockPistonRetractEvent event) {
//    	log("Block piston retract event");
//    	Block block = event.getBlock();
//        World world = block.getWorld();
//        
//    	Location loc = block.getLocation();
//        int X = loc.getBlockX();
//        int Y = loc.getBlockY();
//        int Z = loc.getBlockZ();
//        String pistonkey = prefix + "-" + world.getName() + "." + X + "." + Y + "." + Z;
//        String pistonowner = jedis.get(pistonkey);
//        log("Piston Check: " + pistonkey + "  Owner: " + pistonowner);
//        
//    	// Get the piston's alliance
//    	String pistonalliance = "";
//        if (pistonowner != null && PlayerAlliance.containsKey(pistonowner)) {
//        	pistonalliance = PlayerAlliance.get(pistonowner);
//        } else {
//        	pistonalliance = pistonowner;
//        }
//
//        Block bl = event.getBlock();
//        
//    	Location bloc = bl.getLocation();
//        int bX = bloc.getBlockX();
//        int bY = bloc.getBlockY();
//        int bZ = bloc.getBlockZ();
//        
//        String blockkey = prefix + "-" + world.getName() + "." + bX + "." + bY + "." + bZ;
//        String blockowner = jedis.get(blockkey);
//        log("Block Check: " + blockkey + "  Owner: " + blockowner);
//        
//    	// Get the block's alliance
//    	String blockalliance = "";
//        if (blockowner != null && PlayerAlliance.containsKey(blockowner)) {
//        	blockalliance = PlayerAlliance.get(blockowner);
//        } else {
//        	blockalliance = blockowner;
//        }
//        
//        if (blockalliance != null && !pistonalliance.equalsIgnoreCase(blockalliance)) {
//        	event.setCancelled(true);
//        	log(pistonalliance + " is trying to move " + blockalliance + "'s block with a piston. Cancelling event!");
//        }
//    }
    
    public Boolean isOwner(Player player, Block block) {
        String world = block.getWorld().getName();
        if (Worlds.contains(world)) {
	        int X = block.getX();
	        int Y = block.getY();
	        int Z = block.getZ();
	        String playerkey = prefix + "-" + world + "." + X + "." + Y + "." + Z;
	        
	        // Get the block's owner
	        String owner = jedis.get(playerkey);
	    	
	        // if owner is null, return
	        if (owner == null) {
	        	return true;
	        }

	        // Get the owner's alliance
	    	String owner_alliance = owner;
	        if (PlayerAlliance.containsKey(owner)) {
	        	owner_alliance = PlayerAlliance.get(owner);
	        }

	        // Get the player's alliance
	    	String player_alliance = "";
	        if (PlayerAlliance.containsKey(player.getName())) {
	        	player_alliance = PlayerAlliance.get(player.getName());
	        } else {
	        	player_alliance = player.getName();
	        }

	        // is the player the owner, or is the player's alliance the same as the owner's alliance?
	        if (player.getName().equalsIgnoreCase(owner) || player_alliance.equalsIgnoreCase(owner_alliance)) {
	        	return true;
	        }
	        
	        // if nothing matched, it belongs to someone else
	        return false;
        }
        return true;
    }
    
    public List<String> getSelectionKeys(Selection selection) {
    	List<String> keys = new ArrayList<String>();
	    World world = selection.getWorld();
	    Location min = selection.getMinimumPoint();
	    Location max = selection.getMaximumPoint();
	    int minX = 0;
	    int minY = 0;
	    int minZ = 0;
	    int maxX = 0;
	    int maxY = 0;
	    int maxZ = 0;
	    if (min.getBlockX() <= max.getBlockX()) {
	    	minX = min.getBlockX();
		    maxX = max.getBlockX();
	    } else {
	    	minX = max.getBlockX();
		    maxX = min.getBlockX();
	    }
	    if (min.getBlockY() <= max.getBlockY()) {
	    	minY = min.getBlockY();
		    maxY = max.getBlockY();
	    } else {
	    	minY = max.getBlockY();
		    maxY = min.getBlockY();
	    }
	    if (min.getBlockZ() <= max.getBlockZ()) {
	    	minZ = min.getBlockZ();
		    maxZ = max.getBlockZ();
	    } else {
	    	minZ = max.getBlockZ();
		    maxZ = min.getBlockZ();
	    }
	    for (int x=minX; x<=maxX; x++) {
		    for (int y=minY; y<=maxY; y++) {
    		    for (int z=minZ; z<=maxZ; z++) {
    		    	Location loc = new Location(world, (double) x, (double) y, (double) z);
    		    	Block b = loc.getBlock();
    		    	if (!b.getType().toString().equalsIgnoreCase("AIR")) {
	    		        String playerkey = prefix + "-" + world.getName() + "." + x + "." + y + "." + z;
	    		        keys.add(playerkey);
    		    	}
    		    }
		    }
	    }
	    return keys;
    }
    
    public Hashtable<String, String> getOwner(Block block) {
        Hashtable <String, String> Owner = new Hashtable <String, String>();

        Owner.put("owner", "");
        Owner.put("alliance", "");
        
        String world = block.getWorld().getName();
        if (Worlds.contains(world)) {
	        int X = block.getX();
	        int Y = block.getY();
	        int Z = block.getZ();
	        
	        String playerkey = prefix + "-" + world + "." + X + "." + Y + "." + Z;
	        log("Block Check: " + playerkey);
	        
	        // Get the block's owner
	        String owner = jedis.get(playerkey);
	        if (owner != null) {
	        	Owner.put("owner", owner);
		        
	        	// Get the block owner's alliance
	            if (PlayerAlliance.containsKey(owner)) {
	    	        Owner.put("alliance", PlayerAlliance.get(owner));
	            } else {
	    	        Owner.put("alliance", owner);
	            }
	        }
        }
		return Owner;
    }
    
    public Block isTotem(Block block) {
        // Detect when a totem is built
        Block totem = null;
        if (block.getType().equals(Material.OBSIDIAN) || block.getType().equals(Material.REDSTONE_BLOCK)) {
            World w = block.getWorld();
        	Block above1 = w.getBlockAt(block.getX(), block.getY()+1, block.getZ());
        	Block above2 = w.getBlockAt(block.getX(), block.getY()+2, block.getZ());
        	Block above3 = w.getBlockAt(block.getX(), block.getY()+3, block.getZ());
        	Block below1 = w.getBlockAt(block.getX(), block.getY()-1, block.getZ());
        	Block below2 = w.getBlockAt(block.getX(), block.getY()-2, block.getZ());
        	Block below3 = w.getBlockAt(block.getX(), block.getY()-3, block.getZ());
	        if (block.getType().equals(Material.OBSIDIAN)) {
	        	if (above1.getType().equals(Material.REDSTONE_BLOCK) && above2.getType().equals(Material.REDSTONE_BLOCK) && above3.getType().equals(Material.OBSIDIAN)) {
	        		totem = block;
	        	}
	        	if (below1.getType().equals(Material.REDSTONE_BLOCK) && below2.getType().equals(Material.REDSTONE_BLOCK) && below3.getType().equals(Material.OBSIDIAN)) {
	        		totem = below3;
	        	}
	        }
	        if (block.getType().equals(Material.REDSTONE_BLOCK)) {
	        	if (above1.getType().equals(Material.OBSIDIAN) && below1.getType().equals(Material.REDSTONE_BLOCK) && below2.getType().equals(Material.OBSIDIAN)) {
	        		totem = below2;
	        	}
	        	if (above1.getType().equals(Material.REDSTONE_BLOCK) && above2.getType().equals(Material.OBSIDIAN) && below1.getType().equals(Material.OBSIDIAN)) {
	        		totem = below1;
	        	}
	        }
        }
        return totem;
    }
    
    public boolean opBypass(String player) {
    	User user = ((Essentials) Bukkit.getPluginManager().getPlugin("Essentials")).getUser(player);
    	if (user != null) {
	    	if (user.isGodModeEnabled()) {
		    	log(user.getName() + " has God mode: bypassing restrictions");
	    		return true;
	    	}
	    	if (user.getBase().getGameMode() != GameMode.SURVIVAL) {
		    	log(user.getName() + " is in Creative: bypassing restrictions");
	    		return true;
	    	}
	    	if (user.getBase().isOp()) {
		    	log(user.getName() + " is OP: bypassing restrictions");
	    		return true;
	    	}
    	}
    	return false;
    }
    
    public boolean tpCooldown(String player) {
        if (tpCooldown.containsKey(player)) {
			Long diff = System.currentTimeMillis() - tpCooldown.get(player);
			if (diff < 5000) {
				return true;
			}
		}
        return false;
    }
    
    @EventHandler
    public void onPlayerTeleport(PlayerTeleportEvent event) {
    	Player player = event.getPlayer();
		tpCooldown.put(player.getName(), System.currentTimeMillis());
		if (InBorder.containsKey(player.getName())) {
	    	log(player.getName() + " teleported - removing totem record");
			InBorder.remove(player.getName());
		}
		//Location tpto = event.getTo();
    }
	
	@EventHandler
	public void onPlayerJoin(PlayerJoinEvent event) {
    	Player player = event.getPlayer();
		tpCooldown.put(player.getName(), System.currentTimeMillis());
		if (InBorder.containsKey(player.getName())) {
			InBorder.remove(player.getName());
		}
	}
	
	@EventHandler
	public void onPlayerQuit(PlayerQuitEvent event) {
    	Player player = event.getPlayer();
		if (InBorder.containsKey(player.getName())) {
			InBorder.remove(player.getName());
		}
	}
	
	@EventHandler
	public void onCreatureSpawn(CreatureSpawnEvent event) {
		Location mob_loc = event.getLocation();
    	for (Location loc : Totems.keySet()) {
    		if (MobsEnabled.containsKey(Totems.get(loc)) && !MobsEnabled.get(Totems.get(loc))) {
	    		if (mob_loc.getWorld().equals(loc.getWorld()) && mob_loc.distance(loc) < totem_radius) {
	    			EntityType mob_type = event.getEntityType();
	    			if (PreventMobs.contains(mob_type.toString())) {
    	        		event.setCancelled(true);
    	        		return;
	    			}
//	    	        for (String mt : PreventMobs) {
//	    	        	if (mob_type.toString().equalsIgnoreCase(mt)) {
//	    	        		event.setCancelled(true);
//	    	        		return;
//	    	        	}
//	    	        }
	    		}
    		}
    	}
	}
    
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if (sender instanceof Player) {
            Player player = (Player) sender;
            String current_alliance = null;
            if (PlayerAlliance.containsKey(player.getName())) {
            	current_alliance = PlayerAlliance.get(player.getName());
            }
            if (cmd.getName().equalsIgnoreCase("bp")) {
            	if (args.length == 1) {
	            	if (args[0].equalsIgnoreCase("delowner") && player.hasPermission("bp.setowner")) {
			    		Selection selection = worldEdit.getSelection(player);
			    		if (selection != null) {
			    			if (selection instanceof CuboidSelection) {
			    				Integer count = 0;
					    		for (String key : getSelectionKeys(selection)) {
			    			        jedis.del(key);
			    			        count = count + 1;
					    		}
					    		sender.sendMessage(ChatColor.GOLD + "Removed owner on " + ChatColor.GREEN + count + ChatColor.GOLD + " blocks!");
			    			} else {
		            			sender.sendMessage(ChatColor.RED + "Selection must be a cuboid!");
			    			}
			    		} else {
		        			sender.sendMessage(ChatColor.RED + "No selection found!");
		    			}
            		} else {
            			sender.sendMessage(ChatColor.RED + "Correct usage: /bp delowner");
        			}
	    		}
            	if (args.length == 2) {
	            	if (args[0].equalsIgnoreCase("setowner") && player.hasPermission("bp.setowner")) {
			    		Selection selection = worldEdit.getSelection(player);
			    		if (selection != null) {
			    			if (selection instanceof CuboidSelection) {
			    				Integer count = 0;
					    		for (String key : getSelectionKeys(selection)) {
			    			        jedis.set(key, args[1]);
			    			        count = count + 1;
					    		}
					    		sender.sendMessage(ChatColor.GOLD + "Successfully set owner to " + ChatColor.GREEN + args[1] + ChatColor.GOLD + " on " + ChatColor.GREEN + count + ChatColor.GOLD + " blocks!");
			    			} else {
		            			sender.sendMessage(ChatColor.RED + "Selection must be a cuboid!");
			    			}
			    		} else {
		        			sender.sendMessage(ChatColor.RED + "No selection found!");
		    			}
            		} else if (args[0].equalsIgnoreCase("history") && player.hasPermission("bp.history")) {
            			showHistory(player, args[1]);
	            	} else {
            			sender.sendMessage(ChatColor.RED + "Correct usage: /bp <setowner|history> <player>");
        			}
                }
            } else if (cmd.getName().equalsIgnoreCase("alliance") && player.hasPermission("bp.alliance")) {
                if (args.length == 0) {
                	sender.sendMessage(ChatColor.GRAY + "--- " + ChatColor.GOLD + "Alliance Management" + ChatColor.GRAY + " ---");
                	sender.sendMessage(ChatColor.GRAY + "info: " + ChatColor.GOLD + "/alliance info <name>");
                	sender.sendMessage(ChatColor.GRAY + "create: " + ChatColor.GOLD + "/alliance create <name>");
                	sender.sendMessage(ChatColor.GRAY + "rename: " + ChatColor.GOLD + "/alliance rename <name>");
                	sender.sendMessage(ChatColor.GRAY + "leave: " + ChatColor.GOLD + "/alliance leave <name>");
                	sender.sendMessage(ChatColor.GRAY + "log: " + ChatColor.GOLD + "/alliance log");
                	sender.sendMessage(ChatColor.GRAY + "---");
                	sender.sendMessage(ChatColor.GRAY + "setleader: " + ChatColor.GOLD + "/alliance setleader <player>");
                	sender.sendMessage(ChatColor.GRAY + "invite: " + ChatColor.GOLD + "/alliance invite <player>");
                	sender.sendMessage(ChatColor.GRAY + "kick: " + ChatColor.GOLD + "/alliance kick <player>");
                	sender.sendMessage(ChatColor.GRAY + "---");
                	sender.sendMessage(ChatColor.GRAY + "list: " + ChatColor.GOLD + "/alliance list");
                	sender.sendMessage(ChatColor.GRAY + "monsters: " + ChatColor.GOLD + "/alliance monsters");
                	sender.sendMessage(ChatColor.GRAY + "---");
                } else if (args.length == 1) {
                	if (args[0].equalsIgnoreCase("list")) {
                    	sender.sendMessage(ChatColor.GRAY + "--- " + ChatColor.GOLD + "Alliance List" + ChatColor.GRAY + " ---");
                		for (String alliance : Alliance) {
                        	sender.sendMessage(ChatColor.GRAY + "• " + ChatColor.GOLD + alliance);
                		}
                	} else if (args[0].equalsIgnoreCase("log")) {
                		String alliance = PlayerAlliance.get(player.getName());
            			showAllianceHistory(player, alliance);
                	} else if (args[0].equalsIgnoreCase("setspawn")) {
                		log(player.getLocation().toString());
                	} else if (args[0].equalsIgnoreCase("info")) {
                		if (PlayerAlliance.get(player.getName()) != null) {
	                        try {
	                        	sender.sendMessage(ChatColor.GRAY + "--- " + ChatColor.GOLD + "Alliance: " + PlayerAlliance.get(player.getName()) + ChatColor.GRAY + " ---");
	                			conn = DriverManager.getConnection(getConfig().getString("database.url"), getConfig().getString("database.username"), getConfig().getString("database.password"));
	                	        pst = conn.prepareStatement("SELECT alliance_members.player, alliance.leader FROM alliance INNER JOIN alliance_members ON alliance_members.alliance_id = alliance.alliance_id WHERE alliance.name = ?");
	                	        pst.setString(1, PlayerAlliance.get(player.getName()));
	                			ResultSet res = null;
	                	        res = pst.executeQuery();
	                	        Boolean leader_printed = false;
	                	        while (res.next()) {
	                	        	if (!leader_printed) {
		                	        	sender.sendMessage(ChatColor.GRAY + "• " + ChatColor.GOLD + res.getString("leader") + " (leader)");
		                	        	leader_printed = true;
	                	        	}
	                	        	if (!res.getString("player").equalsIgnoreCase(res.getString("leader"))) {
	                	        		sender.sendMessage(ChatColor.GRAY + "• " + ChatColor.GOLD + res.getString("player"));
	                	        	}
	                	        }
	                		} catch (SQLException e) {
	                			e.printStackTrace();
	                		} finally {
	                	    	try {
	                	        	pst.close();
	                				conn.close();
	                			} catch (SQLException e) {
	                				e.printStackTrace();
	                			}
	                		}
                		} else {
                			sender.sendMessage(ChatColor.RED + "You aren't currently in an alliance!");
                		}
                	} else if (args[0].equalsIgnoreCase("accept")) {
                		if (InvitationTime.containsKey(player.getName())) {
                			Long diff = System.currentTimeMillis() - InvitationTime.get(player.getName());
                			if (diff < 60000) {
    	                        try {
    	                            conn = DriverManager.getConnection(getConfig().getString("database.url"), getConfig().getString("database.username"), getConfig().getString("database.password"));
    	                            pst = conn.prepareStatement("SELECT alliance_id, name, leader FROM alliance WHERE name = ?");
    	                            pst.setString(1, Invitation.get(player.getName()));
    	                            ResultSet res = null;
    	                            res = pst.executeQuery();
    	                            if (res.next()) {
    	                                	Integer alliance_id = res.getInt("alliance_id");
    	                                    pst = conn.prepareStatement("REPLACE INTO alliance_members SET alliance_id = ?, player = ?");
    	                                    pst.setInt(1, alliance_id);
    	                                    pst.setString(2, player.getName());
    	                                    pst.executeUpdate();
    	                        			sender.sendMessage(ChatColor.GOLD + "Joining alliance " + Invitation.get(player.getName()));
    	                        			
    	                        			if (InBorder.containsKey(player.getName())) {
    	                        				InBorder.remove(player.getName());
    	                        			}

	    	                    			for (Player p : Bukkit.getServer().getOnlinePlayers()) {
	    	                    			    if (p.getName().equalsIgnoreCase(res.getString("leader"))) {
	        	                                    history(player.getName(), "accepted invitation", res.getString("name"));
	    	                    			    	p.sendMessage(ChatColor.GREEN + player.getName() + " has accepted your invitation!");
	    	                    			        break;
	    	                    			    }
	    	                    			}
    	                            } else {
    	                            	sender.sendMessage(ChatColor.RED + "That alliance no longer exists!");
    	                            }
    	                		} catch (SQLException e) {
    	                			e.printStackTrace();
    	                		} finally {
    	                	    	try {
    	                	        	pst.close();
    	                				conn.close();
    	                			} catch (SQLException e) {
    	                				e.printStackTrace();
    	                			}
    	                		}
                            	this.loadSettings();
                			} else {
                    			sender.sendMessage(ChatColor.RED + "The invitation to " + Invitation.get(player.getName()) + " has expired!");
                			}
                			Invitation.remove(player.getName());
                			InvitationTime.remove(player.getName());
                		} else {
                			sender.sendMessage(ChatColor.RED + "There are no pending invitations for you to accept!");
                		}
                	} else if (args[0].equalsIgnoreCase("deny")) {
                		if (InvitationTime.containsKey(player.getName())) {
	                        try {
	                            conn = DriverManager.getConnection(getConfig().getString("database.url"), getConfig().getString("database.username"), getConfig().getString("database.password"));
	                            pst = conn.prepareStatement("SELECT leader FROM alliance WHERE name = ?");
	                            pst.setString(1, Invitation.get(player.getName()));
	                            ResultSet res = null;
	                            res = pst.executeQuery();
	                            if (res.next()) {
	                    			for (Player p : Bukkit.getServer().getOnlinePlayers()) {
	                    			    if (p.getName().equalsIgnoreCase(res.getString("leader"))) {
    	                                    history(player.getName(), "denied invitation", Invitation.get(player.getName()));
	                    			    	p.sendMessage(ChatColor.RED + player.getName() + " has denied your invitation!");
	                    			        break;
	                    			    }
	                    			}
	                            }
	                		} catch (SQLException e) {
	                			e.printStackTrace();
	                		} finally {
	                	    	try {
	                	        	pst.close();
	                				conn.close();
	                			} catch (SQLException e) {
	                				e.printStackTrace();
	                			}
	                		}
                			Invitation.remove(player.getName());
                			InvitationTime.remove(player.getName());
                		} else {
                			sender.sendMessage(ChatColor.RED + "There are no pending invitations for you to accept!");
                		}
                	} else if (args[0].equalsIgnoreCase("leave")) {
                		if (current_alliance == null) {
                			sender.sendMessage(ChatColor.RED + "You aren't currently in an alliance!");
                		} else {
	                        try {
	                            conn = DriverManager.getConnection(getConfig().getString("database.url"), getConfig().getString("database.username"), getConfig().getString("database.password"));
	                            pst = conn.prepareStatement("SELECT alliance_id FROM alliance WHERE name = ? AND leader = ?");
	                            pst.setString(1, current_alliance);
	                            pst.setString(2, player.getName());
	                            ResultSet res = null;
	                            res = pst.executeQuery();
	                            if (res.next()) {
	                            	// Player is leader
	                            	Integer alliance_id = res.getInt("alliance_id");
	                            	pst = conn.prepareStatement("SELECT alliance_id FROM alliance_members WHERE alliance_id = ? AND player != ?");
		                            pst.setInt(1, alliance_id);
		                            pst.setString(2, player.getName());
		                            res = pst.executeQuery();
		                            if (res.next()) {
		                            	sender.sendMessage(ChatColor.RED + "You are the leader of " + ChatColor.GOLD + current_alliance + ChatColor.RED + "! Leaving it would orphan the members.");
		                            } else {
	                                    pst = conn.prepareStatement("DELETE FROM alliance_members WHERE player = ?");
	                                    pst.setString(1, player.getName());
	                                    pst.executeUpdate();

	                                    pst = conn.prepareStatement("DELETE FROM alliance WHERE alliance_id = ?");
	                                    pst.setInt(1, alliance_id);
	                                    pst.executeUpdate();

	                                    pst = conn.prepareStatement("DELETE FROM alliance_totems WHERE alliance_id = ?");
	                                    pst.setInt(1, alliance_id);
	                                    pst.executeUpdate();
	                                    
	                                    for (Location totem_loc : Totems.keySet()) {
	                        				World w = totem_loc.getWorld();
	                        	        	Block totem = w.getBlockAt(totem_loc.getBlockX(), totem_loc.getBlockY(), totem_loc.getBlockZ());
	                        	        	Hashtable<String, String> owner = getOwner(totem);
	                        	        	if (owner.get("owner").equalsIgnoreCase(player.getName())) {
		                        	        	Block above1 = w.getBlockAt(totem_loc.getBlockX(), totem_loc.getBlockY()+1, totem_loc.getBlockZ());
		                        	        	Block above2 = w.getBlockAt(totem_loc.getBlockX(), totem_loc.getBlockY()+2, totem_loc.getBlockZ());
		                        	        	Block above3 = w.getBlockAt(totem_loc.getBlockX(), totem_loc.getBlockY()+3, totem_loc.getBlockZ());
		                        				totem.breakNaturally();
		                        				above1.breakNaturally();
		                        				above2.breakNaturally();
		                        				above3.breakNaturally();
	                        	        	}
	                                    }

	                                    history(player.getName(), "dissolved", current_alliance);
	                                    
	                                	sender.sendMessage(ChatColor.GREEN + "Alliance removed!");
		                            }
	                            } else {
	                            	// Player is not leader
                                    pst = conn.prepareStatement("DELETE FROM alliance_members WHERE player = ?");
                                    pst.setString(1, player.getName());
                                    pst.executeUpdate();
                                    
                                    for (Location totem_loc : Totems.keySet()) {
                        				World w = totem_loc.getWorld();
                        	        	Block totem = w.getBlockAt(totem_loc.getBlockX(), totem_loc.getBlockY(), totem_loc.getBlockZ());
                        	        	if (totem != null) {
	                        	        	Hashtable<String, String> owner = getOwner(totem);
	                        	        	if (owner.get("owner").equalsIgnoreCase(player.getName())) {
		                        	        	Block above1 = w.getBlockAt(totem_loc.getBlockX(), totem_loc.getBlockY()+1, totem_loc.getBlockZ());
		                        	        	Block above2 = w.getBlockAt(totem_loc.getBlockX(), totem_loc.getBlockY()+2, totem_loc.getBlockZ());
		                        	        	Block above3 = w.getBlockAt(totem_loc.getBlockX(), totem_loc.getBlockY()+3, totem_loc.getBlockZ());
		                        				totem.breakNaturally();
		                        				above1.breakNaturally();
		                        				above2.breakNaturally();
		                        				above3.breakNaturally();
	
		                                        pst = conn.prepareStatement("DELETE FROM alliance_totems WHERE x = ? AND y = ? AND z = ?");
		                                        pst.setInt(1, totem.getX());
		                                        pst.setInt(2, totem.getY());
		                                        pst.setInt(3, totem.getZ());
		                                        pst.executeUpdate();
	                        	        	}
                        	        	}
                                    }

                                    history(player.getName(), "left", current_alliance);
                                    
                                	sender.sendMessage(ChatColor.GREEN + "You left the alliance!");
	                            }
	                		} catch (SQLException e) {
	                			e.printStackTrace();
	                		} finally {
	                	    	try {
	                	        	pst.close();
	                				conn.close();
	                			} catch (SQLException e) {
	                				e.printStackTrace();
	                			}
	                		}
                        	this.loadSettings();
                		}
                	} else if (args[0].equalsIgnoreCase("monsters")) {
                		if (current_alliance == null) {
                			sender.sendMessage(ChatColor.RED + "You aren't currently in an alliance!");
                		} else {
	                        try {
	                            conn = DriverManager.getConnection(getConfig().getString("database.url"), getConfig().getString("database.username"), getConfig().getString("database.password"));
	                            pst = conn.prepareStatement("SELECT alliance_id FROM alliance WHERE name = ? AND leader = ?");
	                            pst.setString(1, current_alliance);
	                            pst.setString(2, player.getName());
	                            ResultSet res = null;
	                            res = pst.executeQuery();
	                            if (res.next()) {
	                            	// player is the leader
	                            	Integer alliance_id = res.getInt("alliance_id");
	                            	if (MobsEnabled.get(current_alliance)) {
	                                    pst = conn.prepareStatement("UPDATE alliance SET mobs_enabled = 0 WHERE alliance_id = ?");
	                                    pst.setInt(1, alliance_id);
	                                    pst.executeUpdate();
	                                    history(player.getName(), "turned off monsters", current_alliance);
	                                	sender.sendMessage(ChatColor.GREEN + "Monsters are " + ChatColor.RED + "disabled" + ChatColor.GREEN + " within the range of your totems!");
	                            	} else {
	                                    pst = conn.prepareStatement("UPDATE alliance SET mobs_enabled = 1 WHERE alliance_id = ?");
	                                    pst.setInt(1, alliance_id);
	                                    pst.executeUpdate();
	                                    history(player.getName(), "turned on monsters", current_alliance);
	                                	sender.sendMessage(ChatColor.GREEN + "Monsters are " + ChatColor.RED + "enabled" + ChatColor.GREEN + " within the range of your totems!");
	                            	}
	                            } else {
                                	sender.sendMessage(ChatColor.RED + "Only the alliance leader can change this setting!");
	                            }
	                		} catch (SQLException e) {
	                			e.printStackTrace();
	                		} finally {
	                	    	try {
	                	        	pst.close();
	                				conn.close();
	                			} catch (SQLException e) {
	                				e.printStackTrace();
	                			}
	                		}
                        	this.loadSettings();
                		}
                	} else {
                    	sender.sendMessage(ChatColor.RED + "Invalid Command");
                	}
                } else if (args.length == 2) {
                	if (args[0].equalsIgnoreCase("info")) {
                        try {
                            conn = DriverManager.getConnection(getConfig().getString("database.url"), getConfig().getString("database.username"), getConfig().getString("database.password"));
                            pst = conn.prepareStatement("SELECT alliance_id, name FROM alliance WHERE name = ?");
                            pst.setString(1, args[1]);
                            ResultSet res = null;
                            res = pst.executeQuery();
                            if (res.next()) {
	                	        sender.sendMessage(ChatColor.GRAY + "--- " + ChatColor.GOLD + "Alliance: " + res.getString("name") + ChatColor.GRAY + " ---");
	                			pst = conn.prepareStatement("SELECT alliance_members.player, alliance.leader FROM alliance INNER JOIN alliance_members ON alliance_members.alliance_id = alliance.alliance_id WHERE alliance.name = ?");
	                	        pst.setString(1, args[1]);
	                	        res = pst.executeQuery();
	                	        Boolean leader_printed = false;
	                	        while (res.next()) {
	                	        	if (!leader_printed) {
		                	        	sender.sendMessage(ChatColor.GRAY + "• " + ChatColor.GOLD + res.getString("leader") + " (leader)");
		                	        	leader_printed = true;
	                	        	}
	                	        	if (!res.getString("player").equalsIgnoreCase(res.getString("leader"))) {
	                	        		sender.sendMessage(ChatColor.GRAY + "• " + ChatColor.GOLD + res.getString("player"));
	                	        	}
	                	        }
                            }
                		} catch (SQLException e) {
                			e.printStackTrace();
                		} finally {
                	    	try {
                	        	pst.close();
                				conn.close();
                			} catch (SQLException e) {
                				e.printStackTrace();
                			}
                		}
                	} else if (args[0].equalsIgnoreCase("create")) {
                		if (PlayerAlliance.get(player.getName()) != null) {
                			sender.sendMessage(ChatColor.RED + "You're already in an alliance. Leave it before creating another!");
                		} else {
	                        try {
	                            conn = DriverManager.getConnection(getConfig().getString("database.url"), getConfig().getString("database.username"), getConfig().getString("database.password"));
	                            pst = conn.prepareStatement("SELECT alliance_id FROM alliance WHERE name = ?");
	                            pst.setString(1, args[1]);
	                            ResultSet res = null;
	                            res = pst.executeQuery();
	                            if (res.next()) {
	                            	sender.sendMessage(ChatColor.RED + "That alliance already exists!");
	                            } else {
	                                log("Creating new record for " + args[1]);
	                                pst = conn.prepareStatement("INSERT INTO alliance SET name = ?, leader = ?");
	                                pst.setString(1, args[1]);
	                                pst.setString(2, player.getName());
	                                pst.executeUpdate();
	                                
	                                pst = conn.prepareStatement("SELECT alliance_id FROM alliance WHERE name = ?");
	                                pst.setString(1, args[1]);
	                                res = pst.executeQuery();
	                                if (res.next()) {
	                                	Integer alliance_id = res.getInt("alliance_id");
	                                    pst = conn.prepareStatement("REPLACE INTO alliance_members SET alliance_id = ?, player = ?");
	                                    pst.setInt(1, alliance_id);
	                                    pst.setString(2, player.getName());
	                                    pst.executeUpdate();

	                                    history(player.getName(), "created", args[1]);
	                                	sender.sendMessage(ChatColor.GREEN + "Successfully created alliance: " + ChatColor.GOLD + args[1]);
	                                } else {
	                                	sender.sendMessage(ChatColor.RED + "Error adding alliance!");
	                                }
	                            }
	                		} catch (SQLException e) {
	                			e.printStackTrace();
	                		} finally {
	                	    	try {
	                	        	pst.close();
	                				conn.close();
	                			} catch (SQLException e) {
	                				e.printStackTrace();
	                			}
	                		}
                        	this.loadSettings();
                		}
                	} else if (args[0].equalsIgnoreCase("rename")) {
                		String alliance = PlayerAlliance.get(player.getName());
                		if (alliance == null) {
                			sender.sendMessage(ChatColor.RED + "You don't have an alliance to rename!");
                		} else if (!Leader.get(alliance).equalsIgnoreCase(player.getName())) {
                			sender.sendMessage(ChatColor.RED + "Only the leader can rename the alliance!");
                		} else {
	                        try {
	                            conn = DriverManager.getConnection(getConfig().getString("database.url"), getConfig().getString("database.username"), getConfig().getString("database.password"));
	                            pst = conn.prepareStatement("SELECT alliance_id FROM alliance WHERE name = ?");
	                            pst.setString(1, args[1]);
	                            ResultSet res = null;
	                            res = pst.executeQuery();
	                            if (res.next()) {
	                            	sender.sendMessage(ChatColor.RED + "That alliance already exists!");
	                            } else {
	                                log("Renaming " + alliance + " to " + args[1]);
	                                pst = conn.prepareStatement("UPDATE alliance SET name = ? WHERE name = ?");
	                                pst.setString(1, args[1]);
	                                pst.setString(2, alliance);
	                                pst.executeUpdate();
                                	sender.sendMessage(ChatColor.GREEN + "Successfully renamed alliance to: " + ChatColor.GOLD + args[1]);
	                            }
	                		} catch (SQLException e) {
	                			e.printStackTrace();
	                		} finally {
	                	    	try {
	                	        	pst.close();
	                				conn.close();
	                			} catch (SQLException e) {
	                				e.printStackTrace();
	                			}
	                		}
                        	this.loadSettings();
                		}
                	} else if (args[0].equalsIgnoreCase("setleader")) {
                		if (current_alliance == null) {
                			sender.sendMessage(ChatColor.RED + "You aren't currently in an alliance!");
                		} else {
	                        try {
	                            conn = DriverManager.getConnection(getConfig().getString("database.url"), getConfig().getString("database.username"), getConfig().getString("database.password"));
	                            pst = conn.prepareStatement("SELECT alliance_id FROM alliance WHERE name = ? AND leader = ?");
	                            pst.setString(1, current_alliance);
	                            pst.setString(2, player.getName());
	                            ResultSet res = null;
	                            res = pst.executeQuery();
	                            if (res.next()) {
	                            	// player is the leader
	                            	Integer alliance_id = res.getInt("alliance_id");
	                            	pst = conn.prepareStatement("SELECT alliance_id, player FROM alliance_members WHERE alliance_id = ? AND player = ?");
		                            pst.setInt(1, alliance_id);
		                            pst.setString(2, args[1]);
		                            res = pst.executeQuery();
		                            if (res.next()) {
	                                    pst = conn.prepareStatement("UPDATE alliance SET leader = ? WHERE alliance_id = ?");
	                                    pst.setString(1, res.getString("player"));
	                                    pst.setInt(2, alliance_id);
	                                    pst.executeUpdate();
	                                    history(player.getName(), "set leader to " + args[1], current_alliance);
	                                    history(args[1], "promoted to leader", current_alliance);
		                            	sender.sendMessage(ChatColor.GREEN + args[1] + " is now the leader of your alliance!");
		                            } else {
	                                	sender.sendMessage(ChatColor.RED + args[1] + " isn't currently in your alliance!");
		                            }
	                            } else {
                                	sender.sendMessage(ChatColor.RED + "Only the current leader can give the position to another player!");
	                            }
	                		} catch (SQLException e) {
	                			e.printStackTrace();
	                		} finally {
	                	    	try {
	                	        	pst.close();
	                				conn.close();
	                			} catch (SQLException e) {
	                				e.printStackTrace();
	                			}
	                		}
                        	this.loadSettings();
                		}
                	} else if (args[0].equalsIgnoreCase("invite")) {
                		if (current_alliance == null) {
                			sender.sendMessage(ChatColor.RED + "You need to create an alliance before inviting others!");
                		} else {
	                        try {
	                            conn = DriverManager.getConnection(getConfig().getString("database.url"), getConfig().getString("database.username"), getConfig().getString("database.password"));
	                            pst = conn.prepareStatement("SELECT alliance_id FROM alliance WHERE name = ? AND leader = ?");
	                            pst.setString(1, current_alliance);
	                            pst.setString(2, player.getName());
	                            ResultSet res = null;
	                            res = pst.executeQuery();
	                            if (res.next()) {
	                            	pst = conn.prepareStatement("SELECT alliance_id, player FROM alliance_members WHERE player = ?");
		                            pst.setString(1, args[1]);
		                            res = pst.executeQuery();
		                            if (res.next()) {
		                            	sender.sendMessage(ChatColor.RED + args[1] + " already belongs to an alliance!");
		                            } else {
		                    			Player invitee = null;
		                    			for (Player p : Bukkit.getServer().getOnlinePlayers()) {
		                    			    if (p.getName().equalsIgnoreCase(args[1])) {
		                    			    	invitee = p;
		                    			        break;
		                    			    }
		                    			}
		                    			if (invitee != null) {
		                    				Invitation.put(invitee.getName(), current_alliance);
		                    				InvitationTime.put(invitee.getName(), System.currentTimeMillis());
		                                    history(player.getName(), "invited " + invitee.getName(), current_alliance);
		                                    history(invitee.getName(), "invited", current_alliance);
		                    				sender.sendMessage(ChatColor.GREEN + "Invitation sent!");
		    		                        invitee.sendMessage(ChatColor.GREEN + player.getName() + ChatColor.GOLD + " is inviting you to the " + ChatColor.GREEN + current_alliance + ChatColor.GOLD + " alliance.");
		    		                        invitee.sendMessage(ChatColor.GOLD + "Type " + ChatColor.GREEN + "/alliance accept" + ChatColor.GOLD + " to accept the invitation!");
		                    			} else {
		                    				sender.sendMessage(ChatColor.RED + "That player isn't currently online!");
		                    			}
		                            }
	                            } else {
                                	sender.sendMessage(ChatColor.RED + "Only the current leader can give the position to another player!");
	                            }
	                		} catch (SQLException e) {
	                			e.printStackTrace();
	                		} finally {
	                	    	try {
	                	        	pst.close();
	                				conn.close();
	                			} catch (SQLException e) {
	                				e.printStackTrace();
	                			}
	                		}
                		}
                	} else if (args[0].equalsIgnoreCase("kick")) {
                		if (current_alliance == null) {
                			sender.sendMessage(ChatColor.RED + "You aren't currently in an alliance!");
                		} else if (!player.getName().equalsIgnoreCase(args[1])) {
	                        try {
	                            conn = DriverManager.getConnection(getConfig().getString("database.url"), getConfig().getString("database.username"), getConfig().getString("database.password"));
	                            pst = conn.prepareStatement("SELECT alliance_id FROM alliance WHERE name = ? AND leader = ?");
	                            pst.setString(1, current_alliance);
	                            pst.setString(2, player.getName());
	                            ResultSet res = null;
	                            res = pst.executeQuery();
	                            if (res.next()) {
	                            	// player is the leader
	                            	Integer alliance_id = res.getInt("alliance_id");
	                            	pst = conn.prepareStatement("SELECT alliance_id, player FROM alliance_members WHERE alliance_id = ? AND player = ?");
		                            pst.setInt(1, alliance_id);
		                            pst.setString(2, args[1]);
		                            res = pst.executeQuery();
		                            if (res.next()) {
	                                    pst = conn.prepareStatement("DELETE FROM alliance_members WHERE alliance_id = ? AND player = ?");
	                                    pst.setInt(1, alliance_id);
	                                    pst.setString(2, res.getString("player"));
	                                    pst.executeUpdate();
                    			    	if (InBorder.containsKey(args[1])) {
                    			    		InBorder.remove(args[1]);
                    			    	}
	                                    history(player.getName(), "kicked " + args[1], current_alliance);
	                                    history(args[1], "kicked", current_alliance);
		                            	sender.sendMessage(ChatColor.GREEN + args[1] + " has been kicked from your alliance!");
		                            } else {
	                                	sender.sendMessage(ChatColor.RED + args[1] + " isn't currently in your alliance!");
		                            }
	                            } else {
                                	sender.sendMessage(ChatColor.RED + "Only the leader can kick players from the alliance!");
	                            }
	                		} catch (SQLException e) {
	                			e.printStackTrace();
	                		} finally {
	                	    	try {
	                	        	pst.close();
	                				conn.close();
	                			} catch (SQLException e) {
	                				e.printStackTrace();
	                			}
	                		}
                        	this.loadSettings();
                		} else {
                			sender.sendMessage(ChatColor.RED + "You can't kick yourself!");
                		}
                	} else {
                    	sender.sendMessage(ChatColor.RED + "Invalid Command");
                	}
                } else {
                	sender.sendMessage(ChatColor.RED + "Invalid Command");
            	}
            }
        } else if (sender instanceof ConsoleCommandSender) {
            if (cmd.getName().equalsIgnoreCase("alliance")) {
            	if (args.length == 0) {
	            	sender.sendMessage(ChatColor.GRAY + "--- " + ChatColor.GOLD + "Alliance Management" + ChatColor.GRAY + " ---");
	            	sender.sendMessage(ChatColor.GOLD + "/alliance list");
	            	sender.sendMessage(ChatColor.GOLD + "/alliance <alliance>" + ChatColor.GRAY + ": information");
	            	sender.sendMessage(ChatColor.GOLD + "/alliance <player>" + ChatColor.GRAY + ": information");
	            	sender.sendMessage(ChatColor.GRAY + "---");
	            	sender.sendMessage(ChatColor.GOLD + "/alliance <alliance> delete" + ChatColor.GRAY + ": delete the alliance");
	            	sender.sendMessage(ChatColor.GOLD + "/alliance <alliance> givebonus <number>" + ChatColor.GRAY + ": give bonus totems");
	            	sender.sendMessage(ChatColor.GOLD + "/alliance <alliance> setleader <player>" + ChatColor.GRAY + ": set the leader");
	            	sender.sendMessage(ChatColor.GOLD + "/alliance <alliance> kick <player>" + ChatColor.GRAY + ": kick a player");
	            	sender.sendMessage(ChatColor.GOLD + "/alliance <alliance> monsters" + ChatColor.GRAY + ": toggle mobs");
	            	sender.sendMessage(ChatColor.GRAY + "---");
	            } else if (args.length == 1) {
                	if (args[0].equalsIgnoreCase("reload")) {
                		this.loadSettings();
                		sender.sendMessage(ChatColor.GOLD + "BuildProtector config reloaded");
                	} else if (args[0].equalsIgnoreCase("debug")) {
                		if (debug) {
                    		sender.sendMessage(ChatColor.GOLD + "Debug mode disabled");
                			debug = false;
                		} else {
                    		sender.sendMessage(ChatColor.GOLD + "Debug mode enabled");
                			debug = true;
                		}
                	} else if (args[0].equalsIgnoreCase("list")) {
	                	sender.sendMessage(ChatColor.GRAY + "--- " + ChatColor.GOLD + "Alliance List" + ChatColor.GRAY + " ---");
                        try {
                            conn = DriverManager.getConnection(getConfig().getString("database.url"), getConfig().getString("database.username"), getConfig().getString("database.password"));
                            pst = conn.prepareStatement("SELECT name FROM alliance ORDER BY name");
                            ResultSet res = null;
                            res = pst.executeQuery();
                            while (res.next()) {
                            	sender.sendMessage(ChatColor.GRAY + "• " + ChatColor.GOLD + res.getString("name"));
                            }
                		} catch (SQLException e) {
                			e.printStackTrace();
                		} finally {
                	    	try {
                	        	pst.close();
                				conn.close();
                			} catch (SQLException e) {
                				e.printStackTrace();
                			}
                		}
                	} else {
                        try {
                            conn = DriverManager.getConnection(getConfig().getString("database.url"), getConfig().getString("database.username"), getConfig().getString("database.password"));
                            pst = conn.prepareStatement("SELECT alliance_id, name FROM alliance WHERE name = ?");
                            pst.setString(1, args[0]);
                            ResultSet res = null;
                            res = pst.executeQuery();
                            if (res.next()) {
	                	        sender.sendMessage(ChatColor.GRAY + "=== " + ChatColor.GOLD + "Alliance: " + res.getString("name") + ChatColor.GRAY + " ===");
	                			pst = conn.prepareStatement("SELECT alliance_members.player, alliance.leader FROM alliance INNER JOIN alliance_members ON alliance_members.alliance_id = alliance.alliance_id WHERE alliance.name = ? ORDER BY alliance_members.player");
	                	        pst.setString(1, args[0]);
	                	        res = pst.executeQuery();
	                	        Boolean leader_printed = false;
	                	        while (res.next()) {
	                	        	if (!leader_printed) {
		                	        	sender.sendMessage(ChatColor.GRAY + "• " + ChatColor.GOLD + res.getString("leader") + " (leader)");
		                	        	leader_printed = true;
	                	        	}
	                	        	if (!res.getString("player").equalsIgnoreCase(res.getString("leader"))) {
	                	        		sender.sendMessage(ChatColor.GRAY + "• " + ChatColor.GOLD + res.getString("player"));
	                	        	}
	                	        }
                            }
                		} catch (SQLException e) {
                			e.printStackTrace();
                		} finally {
                	    	try {
                	        	pst.close();
                				conn.close();
                			} catch (SQLException e) {
                				e.printStackTrace();
                			}
                		}
                	}
                } else if (args.length == 2) {
                	if (args[0].equalsIgnoreCase("history")) {
                		showHistory(sender, args[1]);
                	}
                	if (args[0].equalsIgnoreCase("log")) {
                		showHistory(sender, args[1]);
                	}
                	// Delete an alliance
                	if (args[0].equalsIgnoreCase("delete")) {
                		
                	}
                }
            }
        }
        return false;
    }
    
    private void history(String player, String action, String alliance) {
        try {
        	conn = DriverManager.getConnection(getConfig().getString("database.url"), getConfig().getString("database.username"), getConfig().getString("database.password"));
            pst = conn.prepareStatement("INSERT INTO alliance_history SET player = ?, action = ?, alliance = ?");
            pst.setString(1, player);
            pst.setString(2, action);
            pst.setString(3, alliance);
            pst.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
	    	try {
	        	pst.close();
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
    }
    
    private void showHistory(CommandSender sender, String player) {
        try {
            conn = DriverManager.getConnection(getConfig().getString("database.url"), getConfig().getString("database.username"), getConfig().getString("database.password"));
            pst = conn.prepareStatement("SELECT DATE_FORMAT(timestamp, '%m/%d/%y %H:%i:%s') AS timestamp, action, alliance FROM alliance_history WHERE player = ? ORDER BY id");
            pst.setString(1, player);
            ResultSet res = null;
            res = pst.executeQuery();
        	sender.sendMessage(ChatColor.RED + "==== Alliance History: " + ChatColor.GOLD + player + ChatColor.RED + " ====");
            while (res.next()) {
        		String timestamp = res.getString("timestamp");
            	String action = res.getString("action");
            	String alliance = res.getString("alliance");
            	sender.sendMessage(ChatColor.RED + "[" + timestamp + "] " + ChatColor.GOLD + action + ChatColor.RED + " - " + ChatColor.GREEN + alliance);
            }
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
	    	try {
	        	pst.close();
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
    }
    
    private void showAllianceHistory(CommandSender sender, String alliance) {
        try {
            conn = DriverManager.getConnection(getConfig().getString("database.url"), getConfig().getString("database.username"), getConfig().getString("database.password"));
            pst = conn.prepareStatement("SELECT DATE_FORMAT(timestamp, '%m/%d/%y %H:%i:%s') AS timestamp, action, player FROM alliance_history WHERE alliance = ? ORDER BY id");
            pst.setString(1, alliance);
            ResultSet res = null;
            res = pst.executeQuery();
        	sender.sendMessage(ChatColor.RED + "==== Alliance History: " + ChatColor.GOLD + alliance + ChatColor.RED + " ====");
            while (res.next()) {
        		String timestamp = res.getString("timestamp");
            	String action = res.getString("action");
            	String player = res.getString("player");
            	sender.sendMessage(ChatColor.RED + "[" + timestamp + "] " + ChatColor.GREEN + player + " " + ChatColor.GOLD + action);
            }
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
	    	try {
	        	pst.close();
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
    }
    
    private void log(String msg) {
    	if (debug) {
    		this.getLogger().info(msg);
    	}
    }
    
    private void log(String msg, Boolean force) {
    	if (debug || force == true) {
    		this.getLogger().info(msg);
    	}
    }
}